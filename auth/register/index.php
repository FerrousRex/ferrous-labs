<?php
/* Display Debug/Error Information */
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/ui/Page.inc.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/LoginFramework.inc.php';

function CleanInput($user_input) {
    $user_input = trim($user_input);
    $user_input = stripslashes($user_input);
    $user_input = htmlspecialchars($user_input);

    return $user_input;
}

/* Validation Error Variables */
$full_name_error = "";
$email_error = "";
$pwd_error = "";
$pwd_confirm_error = "";

/* Validation Booleans */
$full_name_OK = false;
$email_OK = false;
$pwd_OK = false;
$pwd_conf_OK = false;

/* User Input Variables */
$full_name = "";
$last_name = "";
$email_address = "";
$password = "";

function printBody() {
    /* Check if the user has submitted the form */
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        /* Start validation of user input */

        /* Validate User First Name */
        if (empty($_POST["full_name"])) {
            $full_name_error = "Name is required.";
        } else {
            $full_name = CleanInput($_POST["full_name"]);
            $full_name_OK = true;
        }

        /* Clean the email address input early, it gets used
         * in a query.  Better safe than sorry.
         */
        $email_address = CleanInput($_POST["email"]);

        /* Validate Email Address */
        if (empty($_POST["email"])) {
            $email_error = "Email Address is required.";
        } else {
            /* Check if the email is formatted correctly */
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $email_error = "Invalid E-Mail Address.";
            } else {
                /* Check if the email address is already in use */
                if (LoginFramework::EmailAlreadyInUse($_POST["email"])) {
                    $email_error = "Email address is already in use.";
                } else {
                    $email_OK = true;
                }
            }
        }

        /* Password Validation */
        if (empty($_POST["password"])) {
            $pwd_error = "Password is required.";
        } else {
            $password = CleanInput($_POST["password"]);
            $pwd_OK = true;
        }

        /* Password Confirmation Validation */
        if (empty($_POST["password_confirm"])) {
            $pwd_confirm_error = "Re-enter your password.";
        } else {
            if ($_POST["password"] == $_POST["password_confirm"]) {
                if (ValidateResult::ValidatePassword($_POST['password'])) {
                    $pwd_conf_OK = true;
                } else {
                    $pwd_error = "Password does not meet strength requirements.";
                }
            } else {
                $pwd_confirm_error = "Passwords do not match.";
            }
        }

        if ($full_name_OK && $email_OK && $pwd_OK && $pwd_conf_OK) {
            LoginFramework::CreateNewUser($email_address, $password, $full_name);
            $_SESSION['user'] = LoginFramework::LoginUser(htmlentities($_POST['email']), htmlentities($_POST['password']));

            ?>
            <p>
                Your account has been registered, you will redirected to the homepage.  Click <a href="https://eid4emt.umbc.edu">here</a> if you are not redirected.
            </p>
            <script>
                window.location.href = "https://foundry.ferrous-labs.com";
            </script>
            <?php
        }
    }
    ?>
    <h1>Register for an account</h1>
    <hr>
    <p>Please note that passwords must contain at least 1 number, and must be at least 8-characters.</p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div class="reg-form">

            <div class="line">
                <label for="full_name">Name</label>
                <input type="text" name="full_name" value="<?php echo $_POST['full_name']; ?>" /><br>
                <div class='error'><?php echo $full_name_error; ?></div>
            </div>

            <div class="line">
                <label for="email">Email:</label>
                <input type="text" name="email" value="<?php echo $_POST['email']; ?>" /><br>
                <div class='error'><?php echo $email_error; ?></div>
            </div>

            <div class="line">
                <label for="password">Password:</label>
                <input type="password" name="password" value="" /><br>
                <div class='error'><?php echo $pwd_error; ?></div>
            </div>

            <div class="line">
                <label for="password_confirm">Confirm Password:</label>
                <input type="password" name="password_confirm" value="" /><br>
                <div class='error'><?php echo $pwd_confirm_error; ?></div>
            </div>

            <div class="bottom-line">
                <input type="submit" value="Register" />
                <input type="button" value="Cancel" onclick="window.location.href = 'https://foundry.ferrous-labs.com'"/>
            </div>
        </div>
    </form>

    <?php
}

$newPage = new Page("Register User");
$newPage->SetRequiredRole(RoleTypes::NONE);
$newPage->SetContent(printBody);

$newPage->GeneratePage();
