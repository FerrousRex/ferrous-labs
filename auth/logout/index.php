<?php
session_start();

unset($_SESSION['user']);

header("Location: https://foundry.ferrous-labs.com");
die("Logged out - Redirecting to main page");
