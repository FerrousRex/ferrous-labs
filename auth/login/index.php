<?php
/* Display all errors & debug messages */
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/WebCore.inc.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/LoginFramework.inc.php';

$submitted_username = '';

if (!empty($_POST)) {
    $loginOK = LoginFramework::LoginUser($_POST["email"], $_POST["password"]);

    if ($loginOK) {
        $_SESSION["user"] = $loginOK;

        header("Location: https://foundry.ferrous-labs.com");
        die("Redirecting to user account page.");
    } else {
        echo "<b>Invalid email address or password</b>";
        $submitted_username = htmlentities($_POST['email'], ENT_QUOTES, 'UTF-8');
    }
}


  // <input class="textbox" type="text" name="email" value="<?php echo $submitted_username; ?>" />
?>
<html lang="en-US">
  <head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>Login</title>

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link href="/assets/css/site-settings.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/assets/css/semantic.css">

    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/semantic.js"></script>

    <style type="text/css">
        body {
          background-color: #DADADA;
        }
        body > .grid {
          height: 100%;
        }
        .image {
          margin-top: -100px;
        }
        .column {
          max-width: 450px;
        }
      </style>
      <script>
      $(document)
        .ready(function() {
          $('.ui.form')
            .form({
              fields: {
                email: {
                  identifier  : 'email',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Please enter your e-mail'
                    },
                    {
                      type   : 'email',
                      prompt : 'Please enter a valid e-mail'
                    }
                  ]
                },
                password: {
                  identifier  : 'password',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Please enter your password'
                    },
                    {
                      type   : 'length[6]',
                      prompt : 'Your password must be at least 6 characters'
                    }
                  ]
                }
              }
            })
          ;
        })
      ;
      </script>
  </head>
  <body>
    <div class="ui middle aligned center aligned grid">
      <div class="column">
        <h2 class="ui teal image header">
          <img src="assets/images/logo.png" class="image">
          <div class="content">
            Log-in to your account
          </div>
        </h2>
        <form class="ui large form">
          <div class="ui stacked segment">
            <div class="field">
              <div class="ui left icon input">
                <i class="user icon"></i>
                <input type="text" name="email" placeholder="E-mail address">
              </div>
            </div>
            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password" placeholder="Password">
              </div>
            </div>
            <div class="ui fluid large teal submit button">Login</div>
          </div>

          <div class="ui error message"></div>
        </form>
        <div class="ui message">
          Need an account? <a href="#">Sign Up</a>
        </div>
      </div>
    </div>
  </body>
</html>



















    <body>
        <div id="main" class="clearfix">
            <div id="content">
                <h1>Login</h1>
                <form action="" method="post">
                    <fieldset>
                        <legend>Login</legend>
                        <p>
                            <label class="field" for="email">Email Address:</label>

                        </p>
                        <p>
                            <label class="field" for="password">Password: </label>
                            <input type="password" name="password" value="" />
                        </p>
                        <input type="submit" value="Login" />
                    </fieldset>
                </form>
                <a href="/auth/register/">Register</a>
            </div>
        </div>
    </body>
</html>
