<?php
// Uncomment for the original homepage

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/ui/Page.inc.php';

function display_page() {
    ?>
    <blink>
      Hello world!
    </blink>
    <?php
}

$newPage = new Page("UMBC - EHS PACE Learning Management System");
$newPage->SetRequiredRole(RoleTypes::NONE);
$newPage->SetContent(display_page);

$newPage->GeneratePage();
/*
?>
<?php
  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/WebCore.inc.php';

  /* Check if the user is logged in *//*
  if(isset($_SESSION['user'])) {
    ?>
    <p>Welcome back <?php echo htmlspecialchars($_SESSION['user']['full_name']); ?></p>
    <?php
  } else {
    echo "Not current logged in";
  }
?>

<h3>Debug Directory</h3>
<ol>
  <li><a href="/auth/login/">Login</li>
  <li><a href="/auth/logout/">Logout</li>
  <li><a href="/auth/register/">Register</li>
</ol>
*/
