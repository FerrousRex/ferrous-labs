-- Setup code for the Users table
CREATE TABLE pace_users (
	user_id int NOT NULL AUTO_INCREMENT,
	full_name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    email_address varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    primary_phone varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    secondary_phone varchar(255)  COLLATE utf8_unicode_ci,
    address_line_1 varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    address_line_2 varchar(255) COLLATE utf8_unicode_ci,
    city varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    state varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    zip varchar(10) COLLATE utf8_unicode_ci NOT NULL,
    salt char(16) COLLATE utf8_unicode_ci NOT NULL,
    password_hash char(64) COLLATE utf8_unicode_ci NOT NULL,    
    PRIMARY KEY (user_id),
	UNIQUE KEY email_address (email_address)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

-- Setup code for the AccountReset table
-- @todo: can't seem to remember what the type of reset_complete should be (boolean?)
CREATE TABLE AccountReset (
	user_id int NOT NULL,
    reset_key varchar(255),
    reset_complete varchar(255),
    reset_expires datetime,
    PRIMARY KEY (user_id)
);

CREATE TABLE Roles (
	role_id int NOT NULL,
    role_name varchar(255),
    role_description varchar(255),
    PRIMARY KEY (role_id)
);

CREATE TABLE UserRoles (
	user_id int NOT NULL,
    role_id int NOT NULL
);

CREATE TABLE Notifications (
	notification_id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
    created_date datetime NOT NULL,
    description text,
    message text,
    show_in_note_bar bool,
    PRIMARY KEY (notification_id)
);

CREATE TABLE ProductCatalog (
	item_id int NOT NULL AUTO_INCREMENT,
    item_description text,
    item_cost float NOT NULL,
    item_picture text,
    PRIMARY KEY (item_id)
);

CREATE TABLE UserCart (
	user_id int NOT NULL,
    item_id int NOT NULL
);

CREATE TABLE PurchaseHistory (
	transaction_id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    total_cost float NOT NULL,
    transaction_date datetime NOT NULL,
    PRIMARY KEY (transaction_id)
);

CREATE TABLE CourseDates (
	course_id int NOT NULL AUTO_INCREMENT,
    start_date date NOT NULL,
    end_date date NOT NULL,
    max_students int NOT NULL,
    enrolled_students int NOT NULL,
    course_cost float NOT NULL,
    course_type_id int NOT NULL,
    registration_form text NOT NULL,
    PRIMARY KEY (course_id)
);

CREATE TABLE FileStorage (
	file_id int NOT NULL AUTO_INCREMENT,
    file_name text NOT NULL,
    file_mime text NOT NULL,
    file_data blob NOT NULL,
    PRIMARY KEY (file_id)
);

CREATE TABLE CourseType (
	course_id_type int NOT NULL AUTO_INCREMENT,
    course_title text NOT NULL,
    course_description text NOT NULL,
    course_link text NOT NULL,
    PRIMARY KEY (course_id_type)
);

CREATE TABLE CourseHistory (
	course_id int NOT NULL,
    user_id int NOT NULL,
    registration_date date NOT NULL,
    payment_date date NOT NULL,
    remaining_balance float NOT NULL,
    registration_file int NOT NULL
);