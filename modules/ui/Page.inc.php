<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/WebCore.inc.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/LoginFramework.inc.php';

class TemplateConfig {

    public static $css_links = [ ''];
    public static $js_links = [ ''];
    public static $nav_links = [ ''];

}

class Page {

    private $title;
    private $tags;
    private $role_required;
    private $body_content;
    private $header_content;

    public function __construct($set_title) {
        $this->title = $set_title;
    }

    public function SetHeaderContent($set) {
        $this->header_content = $set;
    }

    public function SetContent($set) {
        $this->body_content = $set;
    }

    public function SetRequiredRole($set_role) {
        $this->role_required = $set_role;
    }

    /* @todo: Fix redirects to work for dynamic site trunk */
    public function RedirectPage($redirectTag) {
        switch ($redirectTag) {
            case "home":
                header("Location: https://eid4emt.umbc.edu/");
                break;
            case "login":
                header("Location: https://eid4emt.umbc.edu/webauth/login/");
                break;
            case "logout":
                header("Location: https://eid4emt.umbc.edu/webauth/logout/");
                break;
            case "account":
                header("Location: https://eid4emt.umbc.edu/webauth/account/");
                break;
            case "courses":
                header("Location: https://eid4emt.umbc.edu/courses/");
                break;
            default:
                header("Location: https://eid4emt.umbc.edu/");
                break;
        }
    }

    public function GeneratePage() {

        /* Check if redirect is set */
        $redirectPage = strtolower(htmlspecialchars($_GET["redirect"]));

        if (!empty($redirectPage)) {
            $this->RedirectPage($redirectPage);
        }

        /* An almost head-nod to security */
        if (is_null($this->role_required)) {
            $this->role_required = RoleTypes::ADMIN;
        }

        $user_object = $_SESSION['user'];

        $displayPage = false;

        /* Check if the page requires special permissions */
        if ($this->role_required === RoleTypes::NONE) {
            /* Does NOT require a special role */
            $displayPage = true;
        } else {
            /* Check if the user meets the role requirements */
            $user_in_role = LoginFramework::UserInRole($user_object['email'], $this->role_required);

            if ($user_in_role === true) {
                $displayPage = true;
            } else {
                $displayPage = false;
            }
        }

        if ($displayPage === true) {
            /* For creating cookies or starting downloads */
            if (!empty($this->header_content)) {
                call_user_func($this->header_content);
            }

            echo "<html>";
            echo "<head>";
            $this->GenerateHeader();
            echo "</head>";
            echo "<body>";
            $this->GenerateNavigation();
            $this->GenerateBody();
            $this->GenerateFooter();
            echo "</body>";
            echo "</html>";
        } else {
            echo "<html>";
            echo "<head>";
            echo "<title>Error: 403</title>";
            echo "</head>";
            echo "<body>";
            echo "You do not have sufficient privileges to access this page.";
            echo "</body>";
            echo "</html>";
        }
    }

    protected function GenerateHeader() {
      ?>
      <!-- Standard Meta -->
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

      <!-- Site Properties -->
      <title><?php echo $this->title; ?></title>

      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
      <link href="/assets/css/site-settings.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/assets/css/semantic.css">

      <script src="/assets/js/jquery.js"></script>
      <script src="/assets/js/semantic.js"></script>
      <script>
        $(document)
          .ready(function () {

              $('.browse').popup({
                popup: '.course_list'
              });

              $('.ui.dropdown').dropdown();

              $('.overlay').visibility({
                type: 'fixed',
                offset: 80
              });

              // show dropdown on hover
              $('.main.menu  .ui.dropdown').dropdown({
                on: 'hover'
              });
            });
          </script>
      <?php
    }

    protected function GenerateNavigation() {
      ?>
      <div class="ui container main" style="border-radius: .25em; margin-top: .5em;">
                  <a href="/"><img style="padding: .25em; width: 200px" src="assets/images/logo.png" alt="Emergency Health Services - Professional and Continuing Education"></a>
                  <h1 style="margin: 0;">Professional and Continuing Education</h1>
              </div>

              <div class="ui container menu borderless secondary">
                  <a class="item">News</a>
                  <a class="item">Upcoming Courses</a>

                  <div class="ui borderless secondary menu">
                      <a class="browse item">
                          Training Catalog
                          <i class="dropdown icon"></i>
                      </a>
                  </div>


                  <!-- https://semantic-ui.com/collections/menu.html#dropdown-item -->
                  <div class="ui fluid popup transition hidden course_list">
                      <div class="ui three column equal height divided grid">
                          <div class="column">
                              <h4 class="ui header">In-Person</h4>
                              <div class="ui link list">
                                  <a class="item">ACLS</a>
                                  <a class="item">CPR/AED</a>
                                  <a class="item">CCEMTP</a>
                                  <a class="item">ITLS</a>
                              </div>
                          </div>
                          <div class="column">
                              <h4 class="ui header">In-Person</h4>
                              <div class="ui link list">
                                  <a class="item">Paramedic Refresher</a>
                                  <a class="item">PNCCT</a>
                                  <a class="item">PALS</a>
                              </div>
                          </div>
                          <div class="column">
                              <h4 class="ui header">Online</h4>
                              <div class="ui link list">
                                  <a class="item">Browse Catalog</a>
                              </div>
                          </div>
                      </div>
                  </div>

                  <a class="item">Store</a>
                  <a class="item">Course Policies</a>
                  <a class="item">Contact Us</a>

                  <?php
                    /* Check if the user is logged in */
                    if (isset($_SESSION['user'])) {
                      ?>
                        <a class="right item" onclick="$('.ui.sidebar').sidebar('toggle');"><i class="angle double left icon"></i></a>
                      <?php
                    } else {
                      ?>
                      <a href="/auth/login/" class="item right">Login</a>
                      <?php
                    }
                  ?>
              </div>

              <!-- Account/Custom Info Menu -->
              <div class="ui sidebar inverted right fixed vertical menu">
                  <div class="item">
                      <div class="ui input"><input type="text" placeholder="Search..."></div>
                  </div>
                  <div class="item">
                      Welcome, <?php echo $_SESSION['user']['full_name'] ?>
                  </div>
                  <a class="item" href="/auth/account/">
                    <i class="edit icon"></i> Account
                  </a>
                  <?php /* TODO: Notification class, make red if notifications make green if none? */ ?>
                  <a class="item">
                    Notifications <div class="ui red left pointing label">0</div>
                  </a>
                  <a class="item">
                      <i class="calendar alternate outline icon"></i> Calendar
                  </a>
                  <a class="item" href="/admin/">
                      <i class="settings icon"></i> Admin Panel
                  </a>
                  <a class="item" href="/auth/logout/">
                      <i class="sign-out icon"></i> Logout
                  </a>
                  <div class="ui dropdown item">
                      More
                      <i class="dropdown icon"></i>
                      <div class="menu left">
                          <a class="item"><i class="edit icon"></i> Edit Profile</a>
                          <a class="item"><i class="globe icon"></i> Choose Language</a>
                          <a class="item"><i class="settings icon"></i> Account Settings</a>
                          <a class="item"><i class="shopping cart"></i></a>
                      </div>
                  </div>
              </div>
      <?php
    }

    protected function GenerateBody() {
        echo "<div class='ui container main'>";
        call_user_func($this->body_content);
        echo "</div>";
    }

    protected function GenerateFooter() {
      ?>
      <div class="ui vertical footer segment">
        <div class="ui center aligned container">
          <div class="ui horizontal small divided link list">
            <a class="item" href="#">Site Map</a>
            <a class="item" href="#">Contact Us</a>
            <a class="item" href="#">Terms and Conditions</a>
            <a class="item" href="#">Privacy Policy</a>
            <a class="item" href="https://about.umbc.edu/">About Us</a>
            <a class="item" href="https://umbc.edu/go/equal-opportunity">Equal Opportunity</a>
          </div>
          <div class="ui small copyright">© University of Maryland, Baltimore County • 1000 Hilltop Circle • Baltimore, MD 21250</div>
        </div>
      </div>
      <?php
    }

}
