<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/foundry/modules/core/WebCore.inc.php';

/* TODO: Find a better name for this class */
class Item {
  public $item_id;
  public $item_name;
  public $item_description;
  public $item_cost;
  public $item_picture;
  public $product_category;

  public function __construct($item_id = 0) {
    $this->item_id = $item_id;

    $this->Get();
  }

  public function New() {
    $this->item_id = 0;
    $this->item_name = "";
    $this->item_description = "";
    $this->item_cost  = 0;
    $this->item_picture = "";
    $this->product_category = "";
  }

  public function Get() {

    /* Check if we have a new object or not */
    if($this->item_id != 0) {
      try {
        $db = DB_Connect();

        $selectQuery = $db->prepare("
        SELECT
          item_name,
          item_description,
          item_cost,
          item_picture,
          product_category
        FROM
          ProductCatalog
        WHERE
          item_id = :item_id");
        $selectQuery->bindParam(":item_id", $this->item_id, PDO::PARAM_INT);
        $selectQuery->execute();

        $results = $selectQuery->fetch(PDO::FETCH_ASSOC);

        /* Check if this item has already been registered */
        if(!empty($results['item_id'])) {
          $this->item_name = $results['item_name'];
          $this->item_description = $results['item_description'];
          $this->item_cost  = $results['item_cost'];
          $this->item_picture = $results['item_picture'];
          $this->product_category = $results['product_category'];

          return true;
        } else {
          return false;
        }
      } catch (Exception $selectException) {
        echo "Error: Unable to get the specified item.";
      }
    } else {
      $this->New();
    }
  }

  public function Save() {

    /* I'm sure there's a better way to do this, until then we're going to
       search the database and delete the original (if it exists) and then
       replace it /w the new version
    */
    try {
      $db = DB_Connect();

      $deleteQuery = $db->prepare("DELETE FROM ProductCatalog WHERE item_id = :item_id;");
      $deleteQuery->bindParam(":item_id", $this->item_id, PDO::PARAM_INT);
      $deleteQuery->execute();
    } catch (Exception $deleteException) {
      echo "Error: Unable to delete previous item data.  Did it exist?";
    }

    try {
      $db = DB_Connect();

      $insertQuery = $db->prepare("
      INSERT INTO ProductCatalog (
        item_name,
        item_description,
        item_cost,
        item_picture,
        product_category
      )
      VALUES (
        :item_name,
        :item_description,
        :item_cost,
        :item_picture,
        :product_category
      )");

      $insertQuery->bindValue(":item_name", $this->item_name, PDO::PARAM_STR);
      $insertQuery->bindValue(":item_description", $this->item_description, PDO::PARAM_STR);
      $insertQuery->bindValue(":item_cost", $this->item_cost, PDO::PARAM_STR);
      $insertQuery->bindValue(":item_picture", $this->item_picture, PDO::PARAM_STR);
      $insertQuery->bindValue(":product_category", $this->product_category, PDO::PARAM_STR);

      $result = $insertQuery->execute();
      return $result;

    } catch (Exception $insertException) {
      echo "Error: Unable to insert the new item data.";
    }
  }
}

 ?>
