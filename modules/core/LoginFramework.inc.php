<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/WebCore.inc.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/Register.inc.php';

class Roles {

    public $role_id;
    public $role_name;
    public $role_description;

}

class RoleTypes {

    const NONE = 0;
    const ADMIN = 1;
    const STUDENT = 2;

}

class User {

    public $full_name;
    public $email_address;

}

class PasswordReset {

    public $user_id;
    public $reset_key;
    public $reset_complete;
    public $reset_expires; /* Stores the date the reset was generated, it's a misnomer */

    public function is_expired() {

        $now = new DateTime();
        $passwordTimestamp = new DateTime($this->reset_expires);

        $interval = $now->diff($passwordTimestamp);

        $elapsedDays = (int) $interval->format("%a days");

        if ($elapsedDays <= 1) {
            return false;
        } else {
            return true;
        }
    }

}

class LoginFramework {
    /*     * * SELECT QUERY FUNCTIONS ** */

    public static function IsUserLoggedIn($userName) {

    }

    public static function GetAllUsers() {
        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT email_address AS email_address, full_name, FROM pace_users");
            $selectQuery->execute();

            $results = $selectQuery->fetchAll(PDO::FETCH_CLASS, "User");

            return $results;
        } catch (Exception $ex) {
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    /**
     * Checks against the Users table to see if an email address
     * is already in use
     *
     * @param type $email
     * @return boolean
     */
    public static function EmailAlreadyInUse($email) {

        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT 1 FROM `pace_users` WHERE email_address=:email");
            $selectQuery->bindValue(":email", $email, PDO::PARAM_STR);
            $selectQuery->execute();

            $row = $selectQuery->fetch();

            // If there were any results, then the email address is in use
            if ($row) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    /**
     * Gets the user's id based on their email address, if the email address
     * is not associated with an account an exception is thrown.
     *
     * @param type $email
     * @return type
     * @throws Exception
     */
    private static function GetUserIdFromEmail($email) {

        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT user_id FROM pace_users WHERE email_address=:email");
            $selectQuery->bindValue(":email", $email, PDO::PARAM_STR);
            $selectQuery->execute();

            $results = $selectQuery->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            die("Fatal error connecting to database, please contact your system administrator");
        }

        if (!empty($results['user_id'])) {
            return $results['user_id'];
        } else {
            throw new Exception("User does not exist");
        }
    }

    /**
     * Checks against the UserRoles table to see if the specified user is in the
     * given role
     *
     * @param type $email
     * @param type $role_id
     * @return boolean
     */
    public static function UserInRole($email, $role_id) {
        try {
            $user_id = self::GetUserIdFromEmail($email);
        } catch (Exception $ex) {
            return false;
        }

        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT 1 FROM UserRoles WHERE user_id=:user_id AND role_id=:role_id");
            $selectQuery->bindValue("user_id", $user_id, PDO::PARAM_INT);
            $selectQuery->bindValue("role_id", $role_id, PDO::PARAM_INT);
            $selectQuery->execute();

            $results = $selectQuery->fetch();

            if ($results) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            //die($ex->getMessage());
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    /**
     * Gets an array of Roles that the user belongs to
     *
     * @param type $email
     * @return type
     */
    public static function GetUserRoles($email) {

        $user_id = self::GetUserIdFromEmail($email);

        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT userRoles.role_id, role_name, role_description
                                FROM userRoles
                                INNER JOIN roles ON roles.role_id=userRoles.role_id
                                WHERE userRoles.user_id = :userId");
            $selectQuery->bindValue(":userId", $user_id, PDO::PARAM_INT);
            $selectQuery->execute();

            return $selectQuery->fetchAll(PDO::FETCH_CLASS, 'Roles');
        } catch (Exception $ex) {
            //die($ex->getMessage());
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    public static function AddUserToRole($email, $role_id) {

        $userID = LoginFramework::GetUserIdFromEmail($email);

        try {
            $db = DB_Connect();

            $insertQuery = $db->prepare("INSERT INTO UserRoles (user_id, role_id)
                                        VALUES (:user_id, :role_id)");

            $insertQuery->bindValue(":user_id", $userID, PDO::PARAM_INT);
            $insertQuery->bindValue(":role_id", $role_id, PDO::PARAM_INT);

            $result = $insertQuery->execute();

            return $result;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    /**
     * Validates the supplied credentials against the database,
     * if the user is not found within the database or the credentials
     * are incorrect NULL Is returned.
     *
     * THIS FUNCTION DOES NOT ACTUALLY LOG THE USER IN!
     * It ONLY validates the credentials, to log in the user
     * set the _SESSION["user"] variable equal to the return value
     * of this function.
     *
     * @param type $username
     * @param type $plaintext
     * @return type
     */
    public static function LoginUser($username, $plainText) {
        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT user_id, email_address, full_name, salt, password_hash
                                        FROM pace_users
                                        WHERE email_address = :username");
            $selectQuery->bindValue(":username", $username, PDO::PARAM_STR);
            $selectQuery->execute();
        } catch (Exception $ex) {
            die($ex->getMessage());
            #die("Fatal error connecting to database, please contact your system administrator");
        }

        $results = $selectQuery->fetch(PDO::FETCH_ASSOC);

        if ($results) {
            $check_password = hash('sha256', $plainText . $results['salt']);

            for ($round = 0; $round < 65536; $round++) {
                $check_password = hash('sha256', $check_password . $results['salt']);
            }

            if ($check_password === $results['password_hash']) {
                unset($results['password_hash']);
                unset($results['salt']);

                return $results;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /* INSERT QUERY FUNCTIONS */

    /**
     * Attempts to create a new user for the website.  Performs no safety checks
     * on the information given (No check for duplicate email address, no check
     * for valid email address, No check for empty values).  It is assumed that these
     * are checked at the form level.
     *
     * @param type $email
     * @param type $plainPassword
     * @param type $full_name
     * @return boolean
     */
    public static function CreateNewUser($email, $plainPassword, $full_name) {
        $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
        $password = hash('sha256', $plainPassword . $salt);

        for ($round = 0; $round < 65536; $round++) {
            $password = hash('sha256', $password . $salt);
        }

        try {
            $db = DB_Connect();

            $insertQuery = $db->prepare("INSERT INTO pace_users (email_address, full_name, password_hash, salt)
                                        VALUES (:email, :full_name, :password, :salt)");

            $insertQuery->bindValue(":email", $email, PDO::PARAM_STR);
            $insertQuery->bindValue(":full_name", $full_name, PDO::PARAM_STR);
            $insertQuery->bindValue(":password", $password, PDO::PARAM_STR);
            $insertQuery->bindValue(":salt", $salt, PDO::PARAM_STR);
            $result = $insertQuery->execute();

            return $result;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    /* Password Reset Functions */

    public static function CreatePasswordReset($user_email) {
        try {
            $db = DB_Connect();

            $insertQuery = $db->prepare("REPLACE INTO PasswordResets (user_id, reset_key, reset_complete, reset_expires)
                                        VALUES (:user_id, :reset_key, :reset_complete, :reset_expires)");

            $user_id = self::GetUserIdFromEmail($user_email);
            $reset_expires = new DateTime();

            $insertQuery->bindValue(":user_id", $user_id, PDO::PARAM_STR);
            $insertQuery->bindValue(":reset_key", Register::GenerateResetToken(10), PDO::PARAM_STR);
            $insertQuery->bindValue(":reset_complete", FALSE, PDO::PARAM_BOOL);
            $insertQuery->bindValue(":reset_expires", $reset_expires->format(DateTime::COOKIE), PDO::PARAM_STR);

            $result = $insertQuery->execute();

            return $result;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    public static function GetPasswordReset($user_email) {
        $user_id = self::GetUserIdFromEmail($user_email);

        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT user_id, reset_key, reset_complete, reset_expires
                                FROM PasswordResets
                                WHERE user_id = :user_id");
            $selectQuery->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $selectQuery->execute();

            return $selectQuery->fetchAll(PDO::FETCH_CLASS, 'PasswordReset');
        } catch (Exception $ex) {
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    public static function GetPasswordReset_ByToken($reset_token) {
        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT user_id, reset_key, reset_complete, reset_expires
                                FROM PasswordResets
                                WHERE reset_key = :reset_key");
            $selectQuery->bindValue(":reset_key", $reset_token, PDO::PARAM_STR);
            $selectQuery->execute();

            return $selectQuery->fetchAll(PDO::FETCH_CLASS, 'PasswordReset');
        } catch (Exception $ex) {
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    public static function CompletePassswordReset($user_email) {
        $user_id = self::GetUserIdFromEmail($user_email);

        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("UPDATE PasswordResets
                                SET reset_complete = :reset_complete
                                WHERE user_id = :user_id");
            $selectQuery->bindValue(":reset_complete", TRUE, PDO::PARAM_BOOL);
            $selectQuery->bindValue(":user_id", $user_id, PDO::PARAM_INT);

            $result = $selectQuery->execute();

            return $result;
        } catch (Exception $ex) {
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    public static function CompletePassswordReset_ByToken($reset_token) {
        try {
            $db = DB_Connect();

            /* $updateQuery = $db->prepare("UPDATE PasswordResets
              SET reset_complete = 1
              WHERE reset_key = :reset_key
              LIMIT 1;"); */

            $updateQuery = $db->prepare("UPDATE PasswordResets
                                SET reset_complete = 1
                                WHERE reset_key = :reset_key
                                LIMIT 1;");

            $updateQuery->bindValue(":reset_key", $reset_token, PDO::PARAM_STR);

            return $updateQuery->execute();
        } catch (Exception $ex) {
            die("Fatal error connecting to database, please contact your system administrator");
        }
    }

    public static function ChangeUserPassword($token, $password) {
        /* What do I need in order to complete the password change? */
        $user_id;
        $salt;

        /* Get the user_id first */
        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT user_id
                                FROM PasswordResets
                                WHERE reset_key = :reset_key");
            $selectQuery->bindValue(":reset_key", $token, PDO::PARAM_STR);
            $selectQuery->execute();

            $results = $selectQuery->fetch(PDO::FETCH_ASSOC);

            $user_id = $results['user_id'];
        } catch (Exception $ex) {
            die("Couldn't get the user_id");
        }

        /* Get the password hash next */
        try {
            $db = DB_Connect();

            $selectQuery = $db->prepare("SELECT salt
                                FROM pace_users
                                WHERE user_id = :user_id");
            $selectQuery->bindValue(":user_id", $user_id, PDO::PARAM_STR);
            $selectQuery->execute();

            $results = $selectQuery->fetch(PDO::FETCH_ASSOC);

            $salt = $results['salt'];
        } catch (Exception $ex) {
            die("Couldn't get the hash");
        }

        /* Salt and hash the new password */
        $hashed_password = hash('sha256', $password . $salt);

        for ($round = 0; $round < 65536; $round++) {
            $hashed_password = hash('sha256', $hashed_password . $salt);
        }

        /* Update the user's password entry */
        try {
            $db = DB_Connect();

            $updateQuery = $db->prepare("UPDATE pace_users
                                        SET password_hash = :password
                                        WHERE user_id = :user_id");

            $updateQuery->bindValue(":password", $hashed_password, PDO::PARAM_STR);
            $updateQuery->bindValue(":user_id", $user_id, PDO::PARAM_INT);

            $results = $updateQuery->execute();

            if ($results == true) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
            die("Error updating user entry.");
        }
    }

}
