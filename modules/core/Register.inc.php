<?php
/* Display Debug/Error Information */
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/ui/Page.inc.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/core/LoginFramework.inc.php';

/* Brainstorm time!!!!
 * 
 * So, what if I had a registration class that handled the different tasks like
 *  -> validating user input AND returning specific validation errors
 *  -> sending user emails
 *  -> generating password reset keys
 *  -> handling password resets
 *  -> ...other tasks that I can't think of right now
 */
/* TODO: Yeah, you need to find a better name for that. */

class Register {

    /**
     * Sends an email to user's address notifying them that their registration
     * was successful.
     * 
     * TODO: The message in this function IS ONLY A PLACEHOLDER!
     * 
     * TODO: This function could be altered in the future to include an email
     * verification function.
     * 
     * @param type $user - User's email address
     */
    public static function SendConfirmationEmail($user, $firstName, $lastName) {
        // Multiple recipients
        $to = $user;

        // Subject
        $subject = "Welcome to UMBC's EID Courses!";

        // Message
        $message = "<html><head><title>Welcome to UMBC's EID Courses!</title></head>
            <body>
            <p>Dear $firstName,</p>
            <p>Welcome to UMBC's EID offering, geared towards EMTs.</p>
            </body>
            </html>";

        // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        // Additional headers
        $headers[] = 'To: $firstName $lastName <$user>';
        $headers[] = 'From: EID4EMT <mackd1@umbc.edu>';

        // Mail it
        mail($to, $subject, $message, implode("\r\n", $headers));
    }

    // user_id, reset_key, reset_complete, reset_expires
    public static function SendPasswordReset($emailAddress) {
        
        /* Generate a password reset request */
        LoginFramework::CreatePasswordReset($emailAddress);
        
        $passwordReset = LoginFramework::GetPasswordReset($emailAddress);               
        
        // Subject
        $subject = "Password Reset Link";

        // Message
        $message = "<html><head><title>Password Reset Link</title></head>
            <body>
            <p>Hello,</p>
            <p>Please use the link below to reset your password:</p>
            https://eid4emt.umbc.edu/webauth/account/reset/index.php?reset_token=" . $passwordReset[0]->reset_key . "
            <p>This link will remain valid for 24-hours</p>
            </body>
            </html>";

        // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        // Additional headers
        /* TODO: Change the FROM email, leaving mine there might not be a good idea... */
        $headers[] = 'To: <$emailAddress>';
        $headers[] = 'From: EID4EMT <mackd1@umbc.edu>';

        // Mail it
        mail($emailAddress, $subject, $message, implode("\r\n", $headers));
    }

    public static function GenerateResetToken($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        
        return $randomString;
    }
}

class ValidateResult {

    public $errorMessages;
    public $emailAddress;
    public $firstName;
    public $lastName;
    public $providerNum;
    public $password;

    public function __construct() {
        $this->errorMessages = array();
        $this->emailAddress = '';
        $this->firstName = '';
        $this->lastName = '';
        $this->providerNum = '';
        $this->password = '';
    }

    private function CleanInput($user_input) {
        $user_input = trim($user_input);
        $user_input = stripslashes($user_input);
        $user_input = htmlspecialchars($user_input);

        return $user_input;
    }

    /* WARNING: Make sure you sanitize the user input BEFORE using this function! */
    /* TODO: Would prefer a deny-by-default version instead */

    public function ValidateRegistration() {

        $valid = true;

        /* Check that all of the fields are populated */
        if ($this->emailAddress === "") {
            array_push($this->errorMessages, "Email address must not be empty.");

            $valid = false;
        }

        if ($this->firstName === "") {
            array_push($this->errorMessages, "First name must not be empty.");

            $valid = false;
        }

        if ($this->lastName === "") {
            array_push($this->errorMessages, "Last name must not be empty.");

            $valid = false;
        }

        if ($this->providerNum === "") {
            array_push($this->errorMessages, "Maryland Provider number must not be empty.");

            $valid = false;
        }

        if ($this->password === "") {
            array_push($this->errorMessages, "Password field must not be empty.");

            $valid = false;
        }

        /* Check if the email address is valid or has already been registered */

        if (filter_var($this->emailAddress, FILTER_VALIDATE_EMAIL)) {
            if (LoginFramework::EmailAlreadyInUse($this->emailAddress)) {
                array_push($this->errorMessages, "This Email Address is already in use.");

                $valid = false;
            }
        } else {
            array_push($this->errorMessages, "Email address is not valid.");

            $valid = false;
        }

        /* Check if the Maryland Provider # has already been registered */
        if (LoginFramework::ProviderNumAlreadyInUse($this->providerNum)) {
            array_push($this->errorMessages, "This Maryland Provider Number is already in use.");

            $valid = false;
        }

        return this::ValidatePassword($this->password) && $valid;
    }
    
    public static function ValidatePassword($password) {
        $valid = true;
        
        /* Check password complexity */
        /* Passwords must contain: 1-number, 1-symbol, 8-characters long */
        if (strlen($password) < 8) {
            $valid = false;
        }

        if (!preg_match("#[0-9]+#", $password)) {
            $valid = false;
        }

        if (!preg_match("#[a-zA-Z]+#", $password)) {
             $valid = false;
        }
        
        return $valid;
    }

}
