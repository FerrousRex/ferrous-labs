<?php

/**
 * Contains the configuration information for the database connection
 */
class ConfigInfo {

    const userName = 'ferrousr_pace_connect';
    const password = 'wore pain time master';
    const host = 'localhost';
    const dbName = 'ferrousr_pace_db';
    const dbOptions = 'SET NAMES utf8';

}

/**
 * Returns an open connection to the database specified in the ConfigInfo class
 * @throws Exception
 */
function DB_Connect() {
    try {
        $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => ConfigInfo::dbOptions);
        $db = new PDO("mysql:host=" . ConfigInfo::host . ";dbname=" . ConfigInfo::dbName . ";charset=utf8", ConfigInfo::userName, ConfigInfo::password, $options);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $db;
    } catch (PDOException $ex) {
        throw new Exception("Error: Unable to connect to database.");
    }
}

/* Initialize the default settings for the website */
date_default_timezone_set('America/New_York');
set_include_path($_SERVER['DOCUMENT_ROOT'] . "/modules/");
$arbitrary_versioning_string = "2.3";

if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {

    function undo_magic_quotes_gpc(&$array) {
        foreach ($array as &$value) {
            if (is_array($value)) {
                undo_magic_quotes_gpc($value);
            } else {
                $value = stripslashes($value);
            }
        }
    }

    undo_magic_quotes_gpc($_POST);
    undo_magic_quotes_gpc($_GET);
    undo_magic_quotes_gpc($_COOKIE);
}

header('Content-Type: text/html; charset=utf-8');

if (session_id() == '') {
    session_start();
}
