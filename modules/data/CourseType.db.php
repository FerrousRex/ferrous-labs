<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/foundry/modules/core/WebCore.inc.php';

class CourseType {
  public $course_type_id;
  public $course_title;
  public $course_description;
  public $course_link;
  public $is_ce_course;

  public $ce_type;
  public $ce_amount;

  public function __construct($course_type_id = 0) {
    $this->course_type_id = $course_type_id;

    $this->Get();
  }

  public function New() {
    $this->course_type_id = 0;
    $this->course_title = "";
    $this->course_description = "";
    $this->course_link = "";
    $this->is_ce_course = 0;
    $this->ce_type = "";
    $this->ce_amount = "";
  }

  private function Get() {

    /* Check if we have a new object or not */
    if($this->course_type_id != 0) {
      try {
        $db = DB_Connect();

        $selectQuery = $db->prepare("
        SELECT
          course_type_id,
          course_title,
          course_description,
          course_link,
          is_ce_course,
          ce_type,
          ce_amount
        FROM
          CourseType
        WHERE
          course_type_id = :course_type_id");
        $selectQuery->bindParam(":course_type_id", $this->course_type_id, PDO::PARAM_INT);
        $selectQuery->execute();

        $results = $selectQuery->fetch(PDO::FETCH_ASSOC);

        /* Check if this type ID has been registered */
        if(!empty($results['course_type_id'])) {
          $this->course_title = $results['course_title'];
          $this->course_description = $results['course_description'];
          $this->course_link = $results['course_link'];
          $this->is_ce_course = $results['is_ce_course'];
          $this->ce_type = $results['ce_type'];
          $this->ce_amount = $results['ce_amount'];

          return true;
        } else {
          return false;
        }
      } catch (Exception $selectException) {
        echo "Error: Unable to get the specified Course Type";
      }
    } else {
      $this->New();
    }
  }

  public function Save() {

    /* I'm sure there's a better way to do this, until then we're going to
       search the database and delete the original (if it exists) and then
       replace it /w the new version
    */
    try {
      $db = DB_Connect();

      $deleteQuery = $db->prepare("DELETE FROM CourseType WHERE course_type_id = :course_type_id;");
      $deleteQuery->bindParam(":course_type_id", $this->course_type_id, PDO::PARAM_STR);
      $deleteQuery->execute();
    } catch (Exception $deleteException) {
      echo "Error: Unable to delete previous course type data.  Did it exist?";
    }

    try {
      $db = DB_Connect();

      $insertQuery = $db->prepare("INSERT INTO coursetype (course_title, course_description, course_link, is_ce_course, ce_type, ce_amount)
        VALUES (:course_title, :course_description, :course_link, :is_ce_course, :ce_type, :ce_amount)");

        $insertQuery->bindValue(":course_title", $this->course_title, PDO::PARAM_STR);
        $insertQuery->bindValue(":course_description", $this->course_description, PDO::PARAM_STR);
        $insertQuery->bindValue(":course_link", $this->course_link, PDO::PARAM_STR);
        $insertQuery->bindValue(":is_ce_course", $this->is_ce_course, PDO::PARAM_INT);
        $insertQuery->bindValue(":ce_type", $this->ce_type, PDO::PARAM_STR);
        $insertQuery->bindValue(":ce_amount", $this->ce_amount, PDO::PARAM_INT);

        $result = $insertQuery->execute();

        return $result;
    } catch (Exception $insertException) {
      echo "Error: Unable to insert the new course type data.";
    }
  }
}
